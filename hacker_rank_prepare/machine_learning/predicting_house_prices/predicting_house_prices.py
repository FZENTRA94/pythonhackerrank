"""
LINK: https://www.hackerrank.com/challenges/predicting-house-prices/problem?isFullScreen=true
Objective
In this challenge, we practice using multiple linear regression to predict housing prices.
"""

import pandas as pd
from sklearn.linear_model import LinearRegression


def get_input_data_train_and_test():
    features = []
    target = []
    features_predict = []
    F, N = input().strip().split()
    F, N = int(F), int(N)
    features_names = ["f{}".format(f + 1) for f in range(F)]
    for i in range(N):
        new_row = [float(x) for x in input().strip().split()]
        features.append(new_row[:F])
        target.append(new_row[F])
    data_train = pd.DataFrame(columns=features_names, data=features)
    data_train["y"] = target
    N_test = int(input().strip())
    for i in range(N_test):
        new_row = [float(x) for x in input().strip().split()]
        features_predict.append(new_row)
    data_test = pd.DataFrame(columns=features_names, data=features_predict)

    return data_train, data_test


def train(data_train, target="y"):
    model = LinearRegression()
    model.fit(X=data_train[[f for f in data_train.columns if f != target]], y=data_train[target])
    return model


def predict(data_test, model):
    return model.predict(data_test)


def show_pred(pred):
    for p in pred:
        print(round(p, 2))


if __name__ == '__main__':
    dtrain, dtest = get_input_data_train_and_test()
    model = train(data_train=dtrain)
    prediction = predict(data_test=dtest, model=model)
    show_pred(pred=prediction)
