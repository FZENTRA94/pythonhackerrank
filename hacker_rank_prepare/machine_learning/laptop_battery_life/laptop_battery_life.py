""""
LINK: https://www.hackerrank.com/challenges/battery/problem?isFullScreen=true
Fred is a very predictable man.
For instance, when he uses his laptop, all he does is watch TV shows.
He keeps on watching TV shows until his battery dies.
Also, he is a very meticulous man, i.e. he pays great attention to minute details.
He has been keeping logs of every time he charged his laptop, which includes how long he charged his laptop for
and after that how long was he able to watch the TV. Now, Fred wants to use this log to predict how long
 will he be able to watch TV for when he starts so that he can plan his activities after watching his TV shows accordingly.
"""

from sklearn.linear_model import LinearRegression
import pandas as pd

def trainModel(X,y):
    model = LinearRegression()
    model.fit(X=X, y=y)
    return model

def predict(X, model):
    return model.predict(X)

def main(value):
    data = pd.read_csv('trainingdata.txt', sep=",", header=None)
    data.columns = ["f1", "target"]
    model = trainModel(X=data[["f1"]], y=data["target"])
    x_pred = pd.DataFrame(columns=["f1"], data=[value])
    return round(predict(x_pred, model)[0],2)

if __name__ == '__main__':
    result = main(value=1.50)
    print("Result: {} Ok: {}".format(result, result == 3.00))