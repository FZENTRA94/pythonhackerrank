abc = "ABCDEFJHIJKLMNOPQRSTUVWXYZ"


def f1(x, y, z):
    return x + y - z


if __name__ == '__main__':
    l1 = [1, 10, 20]
    l2 = [2, 20, 40]
    l3 = [5, 10, 15]

    print(list(map(f1, l1, l2, l3)))
