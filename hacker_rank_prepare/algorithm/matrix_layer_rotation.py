#!/bin/python3

import math
import os
import random
import re
import sys
import collections

#
# Complete the 'matrixRotation' function below.
#
# The function accepts following parameters:
#  1. 2D_INTEGER_ARRAY matrix
#  2. INTEGER r
#


def add_new_mov_tuple(current_tuple, new_value):
    if new_value in current_tuple:
        return current_tuple
    return current_tuple.append(new_value)

def matrixRotation(matrix, r):

    # set up rotated matrix
    r_matrix = []
    for i in range(m):
        row = []
        for j in range(n):
            row.append(-1)
        r_matrix.append(row)
    # get number of layers

    #print("Original matrix")
    #print(matrix)

    n_layers = int(min(m, n)/2)


    # Iterate by rotations and layers

    for ln in range(n_layers):
        #print("\tlayer n:", ln)
        i_min = ln
        j_min = ln
        i_max = m - ln - 1
        j_max = n - ln - 1
        #print("\ti min/max: {}/{}".format(i_min, i_max))
        #print("\tj min/max: {}/{}".format(j_min, j_max))
        tuples_mov = []
        for mov in range(0,4):
            if mov == 0:
                "up to down"
                for i in range(i_min, i_max+1):
                    add_new_mov_tuple(tuples_mov, (i, j_min))
            if mov == 1:
                "left to right"
                for j in range(j_min, j_max+1):
                    add_new_mov_tuple(tuples_mov, (i_max, j))
            if mov == 2:
                "down to up"
                for i in range(i_max, i_min-1,-1):
                    add_new_mov_tuple(tuples_mov, (i, j_max))
            if mov == 3:
                "right to left"
                for j in range(j_max, j_min, -1):
                    add_new_mov_tuple(tuples_mov, (i_min,j))
        tuples_mov_shift = collections.deque(tuples_mov)
        tuples_mov_shift.rotate(-r)
        tuples_mov_shift = list(tuples_mov_shift)
        for old_pos, new_pos in zip(tuples_mov, tuples_mov_shift):
            r_matrix[new_pos[0]][new_pos[1]] = matrix[old_pos[0]][old_pos[1]]

    str_matrix = ""
    for row in r_matrix:
        for value in row:
            str_matrix += "{} ".format(value)
        str_matrix += "\n"
    print(str_matrix)
    return


if __name__ == '__main__':
    first_multiple_input = input().rstrip().split()

    m = int(first_multiple_input[0])

    n = int(first_multiple_input[1])

    r = int(first_multiple_input[2])

    matrix = []

    for _ in range(m):
        matrix.append(list(map(int, input().rstrip().split())))

    matrixRotation(matrix, r)
