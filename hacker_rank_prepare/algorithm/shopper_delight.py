import random
import time

def getNumberOfOptionsOld(priceOfJeans, priceOfShoes, priceOfSkirts, priceOfTops, budget):
    options = []

    for j in priceOfJeans:
        for s in priceOfShoes:
            for sk in priceOfSkirts:
                for t in priceOfTops:
                    opt_sum = j + s + sk + t
                    if opt_sum <= budget:
                        options.append((opt_sum))

    return len(options)

def getNumberOfOptions(price_jeans, price_shoes, price_skirts, price_tops, budget):
    """
    """
    n_options = 0
    jeans_shoes = []
    skirts_tops = []
    for j in price_jeans:
        for s in price_shoes:
            jeans_shoes.append(j+s)
    for s in price_skirts:
        for t in price_tops:
            skirts_tops.append(s+t)
    # Create aggregated prices of Jeans-Shoes and Skirts-Tops
    jeans_shoes.sort()
    jeans_shoes.sort(reverse=True)
    # Sort arrays in order to compare price combinations efficiently
    for js in jeans_shoes:
        dif = budget - js
        # Calculate the max money to spend in the next combination of skirts and tops
        if dif <= 0:
            # If no more money, don't try combinations
            continue
        for pos, st in enumerate(skirts_tops):
            if st <= dif:
                n_options += len(skirts_tops) - pos
                # Check the min. combination of skirt-top prices in order to add only the combinations (n combinations = skirt-top - pos) in order that sum(4 products) <= budget
                # In this way avoid try combinations that you priori know that are inside the budget
                break
    return n_options

if __name__ == '__main__':
    print("")
    priceOfJeans = [2, 3]
    priceOfShoes = [4]
    priceOfSkirts = [2, 3]
    priceOfTops = [1, 2]
    budget = 10
    exp_r = 4
    ti = time.time()
    result = getNumberOfOptionsOld(priceOfJeans, priceOfShoes, priceOfSkirts, priceOfTops, budget)
    result2 = getNumberOfOptionsOld(priceOfJeans, priceOfShoes, priceOfSkirts, priceOfTops, budget)
    print("Result 1 = {} ok: {}".format(result, result == exp_r))
    print("Result 2 = {} ok: {}".format(result2, result == exp_r))

    print("\n Opti exp.")
    # Big experiment
    priceOfJeans = []
    priceOfShoes = []
    priceOfSkirts = []
    priceOfTops = []
    for i in range(1, 70):
        priceOfJeans.append(random.randint(1,6))
        priceOfShoes.append(random.randint(1,6))
        priceOfSkirts.append(random.randint(1,6))
        priceOfTops.append(random.randint(1,6))
    budget = 100
    ti = time.time()
    result = getNumberOfOptionsOld(priceOfJeans, priceOfShoes, priceOfSkirts, priceOfTops, budget)
    print("Total time: {} seconds".format(time.time() - ti))
    print(result)

    ti = time.time()
    result = getNumberOfOptions(priceOfJeans, priceOfShoes, priceOfSkirts, priceOfTops, budget)
    print("Total time opti: {} seconds".format(time.time() - ti))
    print(result)
