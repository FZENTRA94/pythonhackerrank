""""
LINK: https://www.hackerrank.com/challenges/bear-and-steady-gene/problem?isFullScreen=true
"""
# !/bin/python3

import math
import os
import random
import re
import sys
import time


#
# Complete the 'steadyGene' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING gene as parameter.
#

GENES = "CGAT"
test_mode = True

def checkSteady(gene):
    for c in GENES:
        if sum([1 for e in gene if e == c]) != int(n / 4):
            return False
    return True

def canSteady(gene):
    for c in GENES:
        if sum([1 for e in gene if e == c]) > n / 4:
            return False
    return True

def steadyGene(gene):

    min_n = n

    if checkSteady(gene=gene):
        return 0
    for start in range(n):
        for end in range(n):
            if start >= end:
                continue
            elif canSteady(gene=gene[0:start] + gene[end:n]):
                new_min = end - start
                if new_min < min_n:
                    min_n = new_min

    return min_n

# Sandbox

if __name__ == '__main__':
    if test_mode:
        #genes_test = ['TGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGC', 'GAAATAAA', 'CGAT']
        genes_test = ['TGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGATTGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGATTGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGATTGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGATTGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGATTGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGATTGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGATTGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGCGAAATAAACGAT']
        for it, gt in enumerate(genes_test):
            n = len(gt)
            print("test ({}): gene = {}, n = {}".format(it + 1, gt, n))
            ti = time.time()
            result = steadyGene(gt)
            print("result:", result)
            print("Total time: {} seconds".format(time.time() - ti))
    else:
        fptr = open(os.environ['OUTPUT_PATH'], 'w')
        n = int(input().strip())
        gene_test = input()
        result = steadyGene(gene_test)
        fptr.write(str(result) + '\n')
        fptr.close()
