""""
LINK: https://www.hackerrank.com/challenges/extra-long-factorials/problem?isFullScreen=true
"""

#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'extraLongFactorials' function below.
#
# The function accepts INTEGER n as parameter.
#

def extraLongFactorials(n):
    result = 1
    if n > 1:
        for i in range(n):
            result *= n-i
    return result

if __name__ == '__main__':
    n = int(input().strip())
    print(extraLongFactorials(n))
