#!/bin/python3

import math
import os
import random
import re
import sys
import numpy as np

#
# Complete the 'formingMagicSquare' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY s as parameter.
#

def getCost(m_original, m_new):
    """"
    Get cost to change element a for b
    """
    csum = 0
    for i in range(len(m_original)):
        for j in range(len(m_original)):
            csum += math.fabs(m_original[i][j] - m_new[i][j])
    return int(csum)

def getMagicConst(s):
    mnumbs = []
    n = len(s)
    for row in s:
        mnumbs.append(sum(row))
    for j in range(n):
        csum = 0
        for row in s:
            csum += row[j]
        mnumbs.append(csum)
    mnumbs.append(sum(s[i][n-i-1] for i in range(n)))
    mnumbs.append(sum(s[i][i] for i in range(n)))

    return set(mnumbs)

def changeRows(s, magic_const):
    new_s = []
    return new_s

def formingMagicSquare(s):
    min_cost = 10*3
    magic_consts = getMagicConst(s)

    return min_cost

if __name__ == '__main__':

    s1 = [[4, 9, 2], [3, 5, 7], [8, 1, 5]]
    #s1 = [[1,1],[1,1]]
    r1 = formingMagicSquare(s=s1)
    print(r1)
    print(r1 == 1)
