#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the flatlandSpaceStations function below.
def flatlandSpaceStations(n, c):
    dists = []
    if n == len(c):
        return 0
    for city in range(n):
        dist = -1
        if city in c:
            dist = 0
        else:
            dist = min(math.fabs(stations - city) for stations in c)
        dists.append(dist)

    return int(max(dists))


if __name__ == '__main__':
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    #nm = input().split()

    #n = int(nm[0])

    #m = int(nm[1])

    #c = list(map(int, input().rstrip().split()))

    n = 5
    c = [0,  4]
    result = flatlandSpaceStations(n, c)
    print(result)
    print(result == 2)

    #fptr.write(str(result) + '\n')

    #fptr.close()
