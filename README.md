# Hacker Rank solutions problems

[HackerRank](https://www.hackerrank.com/dashboard) is a technology hiring platform that is the standard for assessing developer skills for over 2,800+ companies around the world. By enabling tech recruiters and hiring managers to objectively evaluate talent at every stage of the recruiting process, HackerRank helps companies hire skilled developers and innovate faster!

Here I solved some problems (coding, ML and statistics).
